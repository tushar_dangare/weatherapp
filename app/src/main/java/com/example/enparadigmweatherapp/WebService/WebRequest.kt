package com.nta.ts2020.WebService

import android.app.ProgressDialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.util.Log
import com.android.volley.AuthFailureError
import com.android.volley.DefaultRetryPolicy
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.StringRequest
import com.example.enparadigmweatherapp.R
import com.nta.ts2020.WebService.MySingleton.Companion.getInstance
import org.json.JSONObject
import kotlin.jvm.Throws

//        processDialog = new ProgressDialog(mContext);
class WebRequest(private val mContext: Context?) {
    private lateinit var processDialog: ProgressDialog


    private fun showProcess() {
        processDialog = ProgressDialog(mContext)
        processDialog.show()
        processDialog.setCancelable(false)
        val window = processDialog.window
        window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        processDialog.setContentView(R.layout.process_dialog)
    }

    private fun cancelProcess() {
        processDialog.dismiss()
    }

    fun GET_METHOD(url: String?, listener: WebResponseListener, webCallType: WebCallType?, isShowProgress: Boolean) {
        if (mContext != null) {
            if(isShowProgress){
                showProcess()
            }
            val stringRequest = StringRequest(Request.Method.GET, url,
                { response ->
                    run {
                        listener.onResponse(response, webCallType)
                        cancelProcess()
                    }
                },
                { error ->
                    run {
                        listener.onError(error.message)
                        cancelProcess()
                    }
                })
            stringRequest.tag = mContext
            getInstance(mContext)!!.addToRequestQueue(stringRequest)
        } else Log.d(WebRequest::class.java.simpleName, "Something went wrong!")
    }

    fun POST_METHODs(url: String?, params: Map<String, String>, headers: Map<String, String>,
                     listener: WebResponseListener, webCallType: WebCallType?,
                     isShowProgress: Boolean) {
        val request: StringRequest = object : StringRequest(Method.POST, url, Response.Listener { response ->
            cancelProcess()
            listener.onResponse(response, webCallType)
        }, Response.ErrorListener { error ->
            cancelProcess()
            listener.onError(error.toString())
        }) {
            //This is for Headers If You Needed
            @Throws(AuthFailureError::class)
            override fun getHeaders(): Map<String, String> {
                return headers
            }

            //Pass Your Parameters here
            override fun getParams(): Map<String, String> {
                return params
            }
        }
        request.tag = mContext
        getInstance(mContext!!)!!.addToRequestQueue(request)
    }

    fun POST_METHOD(url: String?, requestJson: JSONObject?, webCallType: WebCallType?, hashMap: Map<String, String>?,
                    listener: WebResponseListener, isShowProgress: Boolean) {
        if (mContext != null) {
            if (isShowProgress) showProcess()
            if (hashMap != null) {
                val stringRequest: StringRequest = object : StringRequest(Method.POST, url, Response.Listener { response ->
                    cancelProcess()
                    listener.onResponse(response, webCallType)
                }, Response.ErrorListener { error ->
                    cancelProcess()
                    listener.onError(error.toString())
                }) {
                    @Throws(AuthFailureError::class)
                    override fun getHeaders(): Map<String, String> {
                        return hashMap
                    }
                }
                stringRequest.tag = mContext
                getInstance(mContext)!!.addToRequestQueue(stringRequest)
            } else {
                val jsonObjectRequest = JsonObjectRequest(Request.Method.POST, url,
                        requestJson, { response ->
                    cancelProcess()
                    listener.onResponse(response, webCallType)
                }, { error ->
                    cancelProcess()
                    listener.onError(error.toString())
                })
                // Access the RequestQueue through singleton class.
                jsonObjectRequest.tag = mContext
                getInstance(mContext)!!.addToRequestQueue(jsonObjectRequest)
                jsonObjectRequest.retryPolicy = DefaultRetryPolicy(
                        60000,
                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)
            }
        } else Log.d(WebRequest::class.java.simpleName, "Something went wrong!")
    }

//    fun POST_MULTIPART(url: String?, params: Map<String, String>, multipartParams: Map<String, DataPart>,
//                       listener: WebResponseListener, webCallType: WebCallType?,
//                       isShowProgress: Boolean) {
//        if (mContext != null) {
//            if (isShowProgress) showProcess()
//            //our custom volley request
//            val volleyMultipartRequest: VolleyMultipartRequest = object : VolleyMultipartRequest(Method.POST, url,
//                    Response.Listener { response ->
//                        cancelProcess()
//                        listener.onResponse(String(response.data), webCallType)
//                    },
//                    Response.ErrorListener { error ->
//                        cancelProcess()
//                        listener.onError(error.toString())
//                    }) {
//                @Throws(AuthFailureError::class)
//                override fun getParams(): Map<String, String> {
//                    return params
//                }
//
//                override fun getByteData(): Map<String, DataPart> {
//                    return multipartParams
//                }
//            }
//            volleyMultipartRequest.tag = mContext
//            getInstance(mContext)!!.addToRequestQueue(volleyMultipartRequest)
//            //            volleyMultipartRequest.setRetryPolicy(new DefaultRetryPolicy(
////                    60000,
////                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
////                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
//        } else Log.d(WebRequest::class.java.simpleName, "Something went wrong!")
//    }

    fun CancelRequest() {
        if (mContext != null) {
            val requestQueue = getInstance(mContext)!!.requestQueue
            requestQueue?.cancelAll(mContext)
        }
    }

    fun GROUP_GET_METHOD(url: String?, listener: WebResponseListener, hashMap1: Map<String, String>?, jsonObject1: JSONObject?, webCallType1: WebCallType?, isShowProgress: Boolean) {
        if (isShowProgress) showProcess()
        if (mContext != null) {
            if (isShowProgress) showProcess()
            if (hashMap1 != null) {
                val stringRequest: StringRequest = object : StringRequest(Method.GET, url, Response.Listener { response ->
                    cancelProcess()
                    listener.onResponse(response, webCallType1)
                }, Response.ErrorListener { error ->
                    cancelProcess()
                    listener.onError(error.toString())
                }) {
                    @Throws(AuthFailureError::class)
                    override fun getHeaders(): Map<String, String> {
                        return hashMap1
                    }
                }
                stringRequest.tag = mContext
                getInstance(mContext)!!.addToRequestQueue(stringRequest)
            } else {
                val jsonObjectRequest = JsonObjectRequest(Request.Method.GET, url,
                        jsonObject1, { response ->
                    cancelProcess()
                    listener.onResponse(response, webCallType1)
                }, { error ->
                    cancelProcess()
                    listener.onError(error.toString())
                })
                // Access the RequestQueue through singleton class.
                jsonObjectRequest.tag = mContext
                getInstance(mContext)!!.addToRequestQueue(jsonObjectRequest)
                jsonObjectRequest.retryPolicy = DefaultRetryPolicy(
                        60000,
                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)
            }
        } else Log.d(WebRequest::class.java.simpleName, "Something went wrong!")
    }

}