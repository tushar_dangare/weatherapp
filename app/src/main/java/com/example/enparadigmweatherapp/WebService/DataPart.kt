package com.nta.ts2020.WebService

class DataPart(name: String?, data: ByteArray) {
    var fileName: String? = name
        private set
    var content: ByteArray = data
        private set
    val type: String? = null
}