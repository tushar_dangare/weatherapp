package com.example.enparadigmweatherapp.UI.WeatherMod

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.enparadigmweatherapp.R
import com.example.enparadigmweatherapp.UI.DetailedWeatherMod.DetailedWeatherActivity
import com.example.enparadigmweatherapp.adapters.CityWeatherAdapter
import com.example.enparadigmweatherapp.models.List1
import com.example.enparadigmweatherapp.models.Weather
import com.example.enparadigmweatherapp.utils.Constants
import com.google.gson.Gson
import com.nta.ts2020.WebService.WebCallType
import com.nta.ts2020.WebService.WebRequest
import com.nta.ts2020.WebService.WebResponseListener
import org.json.JSONObject

class WeatherActivity : AppCompatActivity(), CityWeatherAdapter.OnItemClickListener, WebResponseListener {
    lateinit var rvClimate : RecyclerView
    lateinit var cityWeatherAdapter: CityWeatherAdapter
    val list : List<List1> = listOf()
    lateinit var webRequest: WebRequest
    lateinit var gson: Gson
    var lists = mutableListOf<List1>()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        gson = Gson()
        webRequest = WebRequest(this)
        cityWeatherAdapter = CityWeatherAdapter(list,R.layout.weather_card,this,this)
        rvClimate = findViewById(R.id.rv_climate)
        rvClimate.layoutManager = LinearLayoutManager(this)
        rvClimate.adapter = cityWeatherAdapter


        webRequest.GET_METHOD(Constants.URL,this, null,true)

    }


    override fun onError(message: String?) {
        Toast.makeText(this, message,Toast.LENGTH_LONG).show()
    }

    override fun onResponse(response: Any?, webCallType: WebCallType?) {
        Log.d("lo",response.toString())
        val jsonObject = JSONObject(response.toString())
        if(jsonObject.getString("cod") == "200"){
            val weather : Weather = gson.fromJson(jsonObject.toString(),Weather::class.java)
            lists = weather.list
            var temp = 0
            val list1 = mutableListOf<List1>()
            weather.list.forEach {
                if(temp == 0 || temp == 8 || temp == 16 || temp == 24 || temp == 32){
                    list1.add(it)
                }
                temp += 1
            }
            cityWeatherAdapter.notifyUpdate(list1)
        }
    }

    override fun onItemClick(cityWeather: List1?, position: Int, view: View?) {
        val intent = Intent(this, DetailedWeatherActivity::class.java)
        intent.putExtra(Constants.CURRENT_DATA,cityWeather)
        intent.putParcelableArrayListExtra(Constants.DATA, ArrayList(lists))
        startActivity(intent)
    }
}