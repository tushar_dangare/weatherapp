package com.example.enparadigmweatherapp.UI.DetailedWeatherMod

import android.annotation.SuppressLint
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import butterknife.ButterKnife
import com.example.enparadigmweatherapp.R
import com.example.enparadigmweatherapp.models.List1
import com.example.enparadigmweatherapp.utils.ConstantVariables
import com.example.enparadigmweatherapp.utils.Constants
import com.example.enparadigmweatherapp.utils.IconProvider
import com.squareup.picasso.Picasso
import java.util.*

class DetailedWeatherActivity : AppCompatActivity(), View.OnClickListener {
    lateinit var btnBack : ImageView
    var tvDay: TextView? = null

    var textViewWeatherDescription: TextView? = null

    var textViewCurrentTemp: TextView? = null

    var textViewMaxTemp: TextView? = null

    var textViewMinTemp: TextView? = null

    var imageViewWeatherIcon: ImageView? = null

    var textViewDay1: TextView? = null
    var textViewDay2: TextView? = null
    var textViewDay3: TextView? = null
    var textViewDay4: TextView? = null
    var textViewDay5: TextView? = null
    var textViewDay6 :TextView? = null
    var textViewDay7: TextView? = null
    var textViewDay8: TextView? = null

    var imageViewDay1: ImageView? = null
    var imageViewDay2: ImageView? = null
    var imageViewDay3: ImageView? = null
    var imageViewDay4: ImageView? = null
    var imageViewDay5: ImageView? = null
    var imageViewDay6: ImageView? = null
    var imageViewDay7: ImageView? = null
    var imageViewDay8: ImageView? = null


    var textViewMaxTempDay1: TextView? = null
    var textViewMaxTempDay2: TextView? = null
    var textViewMaxTempDay3: TextView? = null
    var textViewMaxTempDay4: TextView? = null
    var textViewMaxTempDay5: TextView? = null
    var textViewMaxTempDay6: TextView? = null
    var textViewMaxTempDay7: TextView? = null
    var textViewMaxTempDay8: TextView? = null


    var textViewMinTempDay1: TextView? = null
    var textViewMinTempDay2: TextView? = null
    var textViewMinTempDay3: TextView? = null
    var textViewMinTempDay4: TextView? = null
    var textViewMinTempDay5: TextView? = null
    var textViewMinTempDay6: TextView? = null
    var textViewMinTempDay7: TextView? = null
    var textViewMinTempDay8: TextView? = null



    var textViewHumidity: TextView? = null

    var textViewWind: TextView? = null

    var textViewCloudiness: TextView? = null

    var textViewPressure: TextView? = null

    private var list1 : List1? = null
    private var lists : MutableList<List1>?= mutableListOf()
    var namesOfDays = arrayOf(
        "SAT", "SUN", "MON", "TUE", "WED", "THU", "FRI"
    )

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detailed_weather)
        ButterKnife.bind(this)

        init()
        if (intent != null) {
            list1 = intent.extras?.getParcelable(Constants.CURRENT_DATA) as List1?
            lists = intent.extras?.getParcelableArrayList<List1>(Constants.DATA) as MutableList<List1>
        }
        setCardData()
    }

    private fun init() {
        btnBack = findViewById(R.id.back_btn)
        btnBack.setOnClickListener(this)

        tvDay = findViewById(R.id.tv_day)
        textViewWeatherDescription = findViewById(R.id.textViewCardWeatherDescription)
        textViewCurrentTemp = findViewById(R.id.textViewCardCurrentTemp)
        textViewCloudiness = findViewById(R.id.textViewCloudiness)
        textViewHumidity = findViewById(R.id.textViewHumidity)
        textViewMaxTemp = findViewById(R.id.textViewCardMaxTemp)
        imageViewWeatherIcon = findViewById(R.id.imageViewCardWeatherIcon)
        textViewMinTemp = findViewById(R.id.textViewCardMinTemp)
        textViewWind = findViewById(R.id.textViewWind)
        textViewPressure = findViewById(R.id.textViewPressure)


        textViewDay1 = findViewById(R.id.textViewDay1)
        textViewDay2 = findViewById(R.id.textViewDay2)
        textViewDay3 = findViewById(R.id.textViewDay3)
        textViewDay4 = findViewById(R.id.textViewDay4)
        textViewDay5 = findViewById(R.id.textViewDay5)
        textViewDay6 = findViewById(R.id.textViewDay6)
        textViewDay7 = findViewById(R.id.textViewDay7)
        textViewDay8 = findViewById(R.id.textViewDay8)

        textViewMaxTempDay1 = findViewById(R.id.textViewMaxTempDay1)
        textViewMaxTempDay2= findViewById(R.id.textViewMaxTempDay2)
        textViewMaxTempDay3 = findViewById(R.id.textViewMaxTempDay3)
        textViewMaxTempDay4 = findViewById(R.id.textViewMaxTempDay4)
        textViewMaxTempDay5 = findViewById(R.id.textViewMaxTempDay5)
        textViewMaxTempDay6 = findViewById(R.id.textViewMaxTempDay6)
        textViewMaxTempDay7 = findViewById(R.id.textViewMaxTempDay7)
        textViewMaxTempDay8 = findViewById(R.id.textViewMaxTempDay8)

        textViewMinTempDay1 = findViewById(R.id.textViewMinTempDay1)
        textViewMinTempDay2 = findViewById(R.id.textViewMinTempDay2)
        textViewMinTempDay3 = findViewById(R.id.textViewMinTempDay3)
        textViewMinTempDay4 = findViewById(R.id.textViewMinTempDay4)
        textViewMinTempDay5 = findViewById(R.id.textViewMinTempDay5)
        textViewMinTempDay6 = findViewById(R.id.textViewMinTempDay6)
        textViewMinTempDay7 = findViewById(R.id.textViewMinTempDay7)
        textViewMinTempDay8 = findViewById(R.id.textViewMinTempDay8)

        imageViewDay1 = findViewById(R.id.imageViewDay1)
        imageViewDay2 = findViewById(R.id.imageViewDay2)
        imageViewDay3 = findViewById(R.id.imageViewDay3)
        imageViewDay4 = findViewById(R.id.imageViewDay4)
        imageViewDay5 = findViewById(R.id.imageViewDay5)
        imageViewDay6 = findViewById(R.id.imageViewDay6)
        imageViewDay7 = findViewById(R.id.imageViewDay7)
        imageViewDay8 = findViewById(R.id.imageViewDay8)
    }

    @SuppressLint("SetTextI18n")
    private fun setCardData() {
        if (list1 != null) {
            Log.d("ju", list1!!.weather[0].description)
            tvDay?.text = ConstantVariables.dateConversion(list1!!.dt)
            textViewWeatherDescription!!.text = list1!!.weather[0].description
            textViewCurrentTemp!!.text =
                ConstantVariables.convertKelvinToCelcius(list1!!.main.temp).toString() + "°"
            textViewMaxTemp!!.text =
                ConstantVariables.convertKelvinToCelcius(list1!!.main.gettemp_max())
                    .toString() + "°"
            textViewMinTemp!!.text =
                ConstantVariables.convertKelvinToCelcius(list1!!.main.gettemp_min())
                    .toString() + "°"
            textViewHumidity?.text = (list1!!.main.humidity.toString() + "%")
            textViewWind?.text = (list1!!.wind.speed.toString() + " m/s")
            textViewCloudiness?.text = (list1!!.clouds.all.toString() + "%")
            textViewPressure?.text = (list1!!.main.pressure.toString() + " hPa")
            val weatherDescription = list1!!.weather[0].main
            Picasso.with(this).load(IconProvider.getImageIcon(weatherDescription))
                .into(imageViewWeatherIcon)
            val date = Date()
            val calendar: Calendar = GregorianCalendar()
        }


        var temp = 1
        lists?.forEach {
            when (temp) {
                    1 -> {
                    textViewDay1!!.text = ConstantVariables.convertMilliSecondsToTime(it.dt)
                    Picasso.with(this).load(IconProvider.getImageIcon(it.weather[0].main))
                        .into(imageViewDay1)
                    textViewMaxTempDay1?.text =
                        ConstantVariables.convertKelvinToCelcius(it.main.gettemp_max())
                            .toString() + "°C"
                    textViewMinTempDay1?.text =
                        ConstantVariables.convertKelvinToCelcius(it.main.gettemp_min())
                            .toString() + "°C"
                }
                2 -> {
                    textViewDay2!!.text = ConstantVariables.convertMilliSecondsToTime(it.dt)
                    Picasso.with(this).load(IconProvider.getImageIcon(it.weather[0].main))
                        .into(imageViewDay2)
                    textViewMaxTempDay2?.text =
                        ConstantVariables.convertKelvinToCelcius(it.main.gettemp_max())
                            .toString() + "°C"
                    textViewMinTempDay2?.text =
                        ConstantVariables.convertKelvinToCelcius(it.main.gettemp_min())
                            .toString() + "°C"
                }
                3 -> {
                    textViewDay3!!.text = ConstantVariables.convertMilliSecondsToTime(it.dt)
                    Picasso.with(this).load(IconProvider.getImageIcon(it.weather[0].main))
                        .into(imageViewDay3)
                    textViewMaxTempDay3?.text =
                        ConstantVariables.convertKelvinToCelcius(it.main.gettemp_max())
                            .toString() + "°C"
                    textViewMinTempDay3?.text =
                        ConstantVariables.convertKelvinToCelcius(it.main.gettemp_min())
                            .toString() + "°C"
                }
                4 -> {
                    textViewDay4!!.text = ConstantVariables.convertMilliSecondsToTime(it.dt)
                    Picasso.with(this).load(IconProvider.getImageIcon(it.weather[0].main))
                        .into(imageViewDay4)
                    textViewMaxTempDay4?.text =
                        ConstantVariables.convertKelvinToCelcius(it.main.gettemp_max())
                            .toString() + "°C"
                    textViewMinTempDay4?.text =
                        ConstantVariables.convertKelvinToCelcius(it.main.gettemp_min())
                            .toString() + "°C"
                }
                5 -> {
                    textViewDay5!!.text = ConstantVariables.convertMilliSecondsToTime(it.dt)
                    Picasso.with(this).load(IconProvider.getImageIcon(it.weather[0].main))
                        .into(imageViewDay5)
                    textViewMaxTempDay5?.text =
                        ConstantVariables.convertKelvinToCelcius(it.main.gettemp_max())
                            .toString() + "°C"
                    textViewMinTempDay5?.text =
                        ConstantVariables.convertKelvinToCelcius(it.main.gettemp_min())
                            .toString() + "°C"
                }
                6 -> {
                    textViewDay6!!.text = ConstantVariables.convertMilliSecondsToTime(it.dt)
                    Picasso.with(this).load(IconProvider.getImageIcon(it.weather[0].main))
                        .into(imageViewDay6)
                    textViewMaxTempDay6?.text =
                        ConstantVariables.convertKelvinToCelcius(it.main.gettemp_max())
                            .toString() + "°C"
                    textViewMinTempDay6?.text =
                        ConstantVariables.convertKelvinToCelcius(it.main.gettemp_min())
                            .toString() + "°C"
                }
                7 -> {
                    textViewDay7!!.text = ConstantVariables.convertMilliSecondsToTime(it.dt)
                    Picasso.with(this).load(IconProvider.getImageIcon(it.weather[0].main))
                        .into(imageViewDay7)
                    textViewMaxTempDay7?.text =
                        ConstantVariables.convertKelvinToCelcius(it.main.gettemp_max())
                            .toString() + "°C"
                    textViewMinTempDay7?.text =
                        ConstantVariables.convertKelvinToCelcius(it.main.gettemp_min())
                            .toString() + "°C"
                }
                8 -> {
                    textViewDay8!!.text = ConstantVariables.convertMilliSecondsToTime(it.dt)
                    Picasso.with(this).load(IconProvider.getImageIcon(it.weather[0].main))
                        .into(imageViewDay8)
                    textViewMaxTempDay8?.text =
                        ConstantVariables.convertKelvinToCelcius(it.main.gettemp_max())
                            .toString() + "°C"
                    textViewMinTempDay8?.text =
                        ConstantVariables.convertKelvinToCelcius(it.main.gettemp_min())
                            .toString() + "°C"
                }
            }
            temp += 1
        }
    }
//        for (i in 1 until lists!!.size) {
////            calendar.time = date
////            val day = namesOfDays[(calendar[Calendar.DAY_OF_WEEK] + i) % 7]
////            var weatherWeekDescription: String?
//            when (i) {
//                1 -> {
//                    println("CASE 1 ----$i")
//                    textViewDay1!!.text = day
//                    weatherWeekDescription =
//                        cityWeather!!.weeklyWeather[i].weatherDetails[0].shotDescription
//                    Picasso.with(this).load(IconProvider.getImageIcon(weatherWeekDescription))
//                        .into(imageViewDay1)
//                    textViewMaxTempDay1?.text =
//                        (cityWeather!!.weeklyWeather[i].temp.max.toString() + "°C")
//                    textViewMinTempDay1?.text =
//                        (cityWeather!!.weeklyWeather[i].temp.min.toString() + "°C")
//                }
//                2 -> {
//                    println("CASE 2 ----$i")
//                    textViewDay2!!.text = day
//                    weatherWeekDescription =
//                        cityWeather!!.weeklyWeather[i].weatherDetails[0].shotDescription
//                    Picasso.with(this).load(IconProvider.getImageIcon(weatherWeekDescription))
//                        .into(imageViewDay2)
//                    textViewMaxTempDay2?.text =
//                        (cityWeather!!.weeklyWeather[i].temp.max.toString() + "°C")
//                    textViewMinTempDay2?.text =
//                        (cityWeather!!.weeklyWeather[i].temp.min.toString() + "°C")
//                }
//                3 -> {
//                    textViewDay3!!.text = day
//                    weatherWeekDescription =
//                        cityWeather!!.weeklyWeather[i].weatherDetails[0].shotDescription
//                    Picasso.with(this).load(IconProvider.getImageIcon(weatherWeekDescription))
//                        .into(imageViewDay3)
//                    textViewMaxTempDay3?.text = (
//                            cityWeather!!.weeklyWeather[i].temp.max.toString() + "°C")
//                    textViewMinTempDay3?.text = (
//                            cityWeather!!.weeklyWeather[i].temp.min.toString() + "°C")
//                }
//                4 -> {
//                    textViewDay4!!.text = day
//                    weatherWeekDescription =
//                        cityWeather!!.weeklyWeather[i].weatherDetails[0].shotDescription
//                    Picasso.with(this).load(IconProvider.getImageIcon(weatherWeekDescription))
//                        .into(imageViewDay4)
//                    textViewMaxTempDay4?.text = (
//                            cityWeather!!.weeklyWeather[i].temp.max.toString() + "°C")
//                    textViewMinTempDay4?.text = (
//                            cityWeather!!.weeklyWeather[i].temp.min.toString() + "°C"
//                            )
//                }
//                5 -> {
//                    textViewDay5!!.text = day
//                    weatherWeekDescription =
//                        cityWeather!!.weeklyWeather[i].weatherDetails[0].shotDescription
//                    Picasso.with(this).load(IconProvider.getImageIcon(weatherWeekDescription))
//                        .into(imageViewDay5)
//                    textViewMaxTempDay5?.text =
//                        (cityWeather!!.weeklyWeather[i].temp.max.toString() + "°C")
//                    textViewMinTempDay5?.text = (
//                            cityWeather!!.weeklyWeather[i].temp.min.toString() + "°C")
//                }
//                6 -> {
//                    textViewDay5!!.text = day
//                    weatherWeekDescription =
//                        cityWeather!!.weeklyWeather[i].weatherDetails[0].shotDescription
//                    Picasso.with(this).load(IconProvider.getImageIcon(weatherWeekDescription))
//                        .into(imageViewDay5)
//                    textViewMaxTempDay5?.text =
//                        (cityWeather!!.weeklyWeather[i].temp.max.toString() + "°C")
//                    textViewMinTempDay5?.text = (
//                            cityWeather!!.weeklyWeather[i].temp.min.toString() + "°C")
//                }
//                7 -> {
//                    textViewDay5!!.text = day
//                    weatherWeekDescription =
//                        cityWeather!!.weeklyWeather[i].weatherDetails[0].shotDescription
//                    Picasso.with(this).load(IconProvider.getImageIcon(weatherWeekDescription))
//                        .into(imageViewDay5)
//                    textViewMaxTempDay5?.text =
//                        (cityWeather!!.weeklyWeather[i].temp.max.toString() + "°C")
//                    textViewMinTempDay5?.text = (
//                            cityWeather!!.weeklyWeather[i].temp.min.toString() + "°C")
//                }
//                8 -> {
//                    textViewDay5!!.text = day
//                    weatherWeekDescription =
//                        cityWeather!!.weeklyWeather[i].weatherDetails[0].shotDescription
//                    Picasso.with(this).load(IconProvider.getImageIcon(weatherWeekDescription))
//                        .into(imageViewDay5)
//                    textViewMaxTempDay5?.text =
//                        (cityWeather!!.weeklyWeather[i].temp.max.toString() + "°C")
//                    textViewMinTempDay5?.text = (
//                            cityWeather!!.weeklyWeather[i].temp.min.toString() + "°C")
//                }
//                else -> {
//                }
//            }
//        }
    override fun onClick(v: View?) {
        if(v?.id == R.id.back_btn){
            onBackPressed()
        }
    }

}