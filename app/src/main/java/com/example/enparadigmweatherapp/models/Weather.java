
package com.example.enparadigmweatherapp.models;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

public class Weather implements Parcelable {

    private String cod;
    private Integer message;
    private Integer cnt;
    private java.util.List<List1> list = null;
    private City city;
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    protected Weather(Parcel in) {
        cod = in.readString();
        if (in.readByte() == 0) {
            message = null;
        } else {
            message = in.readInt();
        }
        if (in.readByte() == 0) {
            cnt = null;
        } else {
            cnt = in.readInt();
        }
        list = in.createTypedArrayList(List1.CREATOR);
        city = in.readParcelable(City.class.getClassLoader());
    }

    public static final Creator<Weather> CREATOR = new Creator<Weather>() {
        @Override
        public Weather createFromParcel(Parcel in) {
            return new Weather(in);
        }

        @Override
        public Weather[] newArray(int size) {
            return new Weather[size];
        }
    };

    public String getCod() {
        return cod;
    }

    public void setCod(String cod) {
        this.cod = cod;
    }

    public Integer getMessage() {
        return message;
    }

    public void setMessage(Integer message) {
        this.message = message;
    }

    public Integer getCnt() {
        return cnt;
    }

    public void setCnt(Integer cnt) {
        this.cnt = cnt;
    }

    public java.util.List<List1> getList() {
        return list;
    }

    public void setList(java.util.List<List1> list) {
        this.list = list;
    }

    public City getCity() {
        return city;
    }

    public void setCity(City city) {
        this.city = city;
    }

    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(cod);
        if (message == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(message);
        }
        if (cnt == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(cnt);
        }
        dest.writeTypedList(list);
        dest.writeParcelable(city, flags);
    }
}
