
package com.example.enparadigmweatherapp.models;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

public class List1 implements Parcelable {

    private Long dt;
    private Main main;
    private java.util.List<Weather_> weather = null;
    private Clouds clouds;
    private Wind wind;
    private Integer visibility;
    private Double pop;
    private Rain rain;
    private Sys sys;
    private String dtTxt;


    protected List1(Parcel in) {
        if (in.readByte() == 0) {
            dt = null;
        } else {
            dt = in.readLong();
        }
        main = in.readParcelable(Main.class.getClassLoader());
        weather = in.createTypedArrayList(Weather_.CREATOR);
        clouds = in.readParcelable(Clouds.class.getClassLoader());
        wind = in.readParcelable(Wind.class.getClassLoader());
        if (in.readByte() == 0) {
            visibility = null;
        } else {
            visibility = in.readInt();
        }
        if (in.readByte() == 0) {
            pop = null;
        } else {
            pop = in.readDouble();
        }
        rain = in.readParcelable(Rain.class.getClassLoader());
        sys = in.readParcelable(Sys.class.getClassLoader());
        dtTxt = in.readString();
    }

    public static final Creator<List1> CREATOR = new Creator<List1>() {
        @Override
        public List1 createFromParcel(Parcel in) {
            return new List1(in);
        }

        @Override
        public List1[] newArray(int size) {
            return new List1[size];
        }
    };

    public Long getDt() {
        return dt;
    }

    public void setDt(Long dt) {
        this.dt = dt;
    }

    public Main getMain() {
        return main;
    }

    public void setMain(Main main) {
        this.main = main;
    }

    public java.util.List<Weather_> getWeather() {
        return weather;
    }

    public void setWeather(java.util.List<Weather_> weather) {
        this.weather = weather;
    }

    public Clouds getClouds() {
        return clouds;
    }

    public void setClouds(Clouds clouds) {
        this.clouds = clouds;
    }

    public Wind getWind() {
        return wind;
    }

    public void setWind(Wind wind) {
        this.wind = wind;
    }

    public Integer getVisibility() {
        return visibility;
    }

    public void setVisibility(Integer visibility) {
        this.visibility = visibility;
    }

    public Double getPop() {
        return pop;
    }

    public void setPop(Double pop) {
        this.pop = pop;
    }

    public Rain getRain() {
        return rain;
    }

    public void setRain(Rain rain) {
        this.rain = rain;
    }

    public Sys getSys() {
        return sys;
    }

    public void setSys(Sys sys) {
        this.sys = sys;
    }

    public String getDtTxt() {
        return dtTxt;
    }

    public void setDtTxt(String dtTxt) {
        this.dtTxt = dtTxt;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        if (dt == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeLong(dt);
        }
        dest.writeParcelable(main, flags);
        dest.writeTypedList(weather);
        dest.writeParcelable(clouds, flags);
        dest.writeParcelable(wind, flags);
        if (visibility == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(visibility);
        }
        if (pop == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeDouble(pop);
        }
        dest.writeParcelable(rain, flags);
        dest.writeParcelable(sys, flags);
        dest.writeString(dtTxt);
    }
}
