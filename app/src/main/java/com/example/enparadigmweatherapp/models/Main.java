
package com.example.enparadigmweatherapp.models;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

public class Main implements Parcelable {

    private Double temp;
    private Double feels_like;
    private Double temp_min;
    private Double temp_max;
    private Integer pressure;
    private Integer sea_level;
    private Integer grnd_level;
    private Integer humidity;
    private Double temp_kf;
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    protected Main(Parcel in) {
        if (in.readByte() == 0) {
            temp = null;
        } else {
            temp = in.readDouble();
        }
        if (in.readByte() == 0) {
            feels_like = null;
        } else {
            feels_like = in.readDouble();
        }
        if (in.readByte() == 0) {
            temp_min = null;
        } else {
            temp_min = in.readDouble();
        }
        if (in.readByte() == 0) {
            temp_max = null;
        } else {
            temp_max = in.readDouble();
        }
        if (in.readByte() == 0) {
            pressure = null;
        } else {
            pressure = in.readInt();
        }
        if (in.readByte() == 0) {
            sea_level = null;
        } else {
            sea_level = in.readInt();
        }
        if (in.readByte() == 0) {
            grnd_level = null;
        } else {
            grnd_level = in.readInt();
        }
        if (in.readByte() == 0) {
            humidity = null;
        } else {
            humidity = in.readInt();
        }
        if (in.readByte() == 0) {
            temp_kf = null;
        } else {
            temp_kf = in.readDouble();
        }
    }

    public static final Creator<Main> CREATOR = new Creator<Main>() {
        @Override
        public Main createFromParcel(Parcel in) {
            return new Main(in);
        }

        @Override
        public Main[] newArray(int size) {
            return new Main[size];
        }
    };

    public Double getTemp() {
        return temp;
    }

    public void setTemp(Double temp) {
        this.temp = temp;
    }

    public Double getfeels_like() {
        return feels_like;
    }

    public void setfeels_like(Double feels_like) {
        this.feels_like = feels_like;
    }

    public Double gettemp_min() {
        return temp_min;
    }

    public void settemp_min(Double temp_min) {
        this.temp_min = temp_min;
    }

    public Double gettemp_max() {
        return temp_max;
    }

    public void settemp_max(Double temp_max) {
        this.temp_max = temp_max;
    }

    public Integer getPressure() {
        return pressure;
    }

    public void setPressure(Integer pressure) {
        this.pressure = pressure;
    }

    public Integer getsea_level() {
        return sea_level;
    }

    public void setsea_level(Integer sea_level) {
        this.sea_level = sea_level;
    }

    public Integer getgrnd_level() {
        return grnd_level;
    }

    public void setgrnd_level(Integer grnd_level) {
        this.grnd_level = grnd_level;
    }

    public Integer getHumidity() {
        return humidity;
    }

    public void setHumidity(Integer humidity) {
        this.humidity = humidity;
    }

    public Double gettemp_kf() {
        return temp_kf;
    }

    public void settemp_kf(Double temp_kf) {
        this.temp_kf = temp_kf;
    }

    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        if (temp == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeDouble(temp);
        }
        if (feels_like == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeDouble(feels_like);
        }
        if (temp_min == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeDouble(temp_min);
        }
        if (temp_max == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeDouble(temp_max);
        }
        if (pressure == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(pressure);
        }
        if (sea_level == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(sea_level);
        }
        if (grnd_level == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(grnd_level);
        }
        if (humidity == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(humidity);
        }
        if (temp_kf == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeDouble(temp_kf);
        }
    }
}
