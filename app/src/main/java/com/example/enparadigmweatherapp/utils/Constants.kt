package com.example.enparadigmweatherapp.utils

object Constants {
    const val URL = "https://api.openweathermap.org/data/2.5/forecast?q=bengaluru&appid=1b80dacc62cf839967c0f6c6f47b4458"
    const val DATA = "data"
    const val CURRENT_DATA = "current_data"
}