package com.example.enparadigmweatherapp.utils

import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*


@Suppress("NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")
class ConstantVariables {
    companion object{
        @JvmStatic
        fun dateConversion(date1: Long): String{
//            val dateStr = "Jul 16, 2013 12:08:59 AM"
//            val df = SimpleDateFormat("MMM dd, yyyy HH:mm:ss a", Locale.ENGLISH)
//            df.timeZone = TimeZone.getTimeZone("UTC")
//           // val date: Date = df.parse(dateStr)
//            df.timeZone = TimeZone.getDefault()
//            val formattedDate: String = df.format(date)


            val date = Date()
            date.time = date1*1000
            return SimpleDateFormat("E, dd MMM").format(date)
        }
        @JvmStatic
        fun convertKelvinToCelcius(fahrenheit: Double): Double {
            return "%.1f".format(fahrenheit - 273.15).toDouble()
        }
        @JvmStatic
        fun convertMilliSecondsToTime(time: Long) : String {
            val formatter: DateFormat = SimpleDateFormat("hh:mm a", Locale.US)
            formatter.timeZone = TimeZone.getTimeZone("UTC")
            return formatter.format(Date(time*1000))
        }
    }
}