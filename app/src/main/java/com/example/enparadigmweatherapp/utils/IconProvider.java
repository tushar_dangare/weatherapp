package com.example.enparadigmweatherapp.utils;

import com.example.enparadigmweatherapp.R;

/**
 * Created by MartinRuiz on 8/21/2017.
 */

public class IconProvider {

    public static int getImageIcon(String weatherDescription){
        int weatherIcon ;
        switch(weatherDescription) {
            case "Thunderstorm":
            case "Atmosphere":
                weatherIcon = R.drawable.ic_atmosphere;
                break;
            case "Drizzle":
                weatherIcon = R.drawable.ic_drizzle;
                break;
            case "Rain":
                weatherIcon = R.drawable.ic_rain;
                break;
            case "Snow":
                weatherIcon = R.drawable.ic_snow;
                break;
            case "Clear":
                weatherIcon = R.drawable.ic_clear;
                break;
            case "Clouds":
                weatherIcon = R.drawable.ic_cloudy;
                break;
            case "Extreme":
                weatherIcon = R.drawable.ic_extreme;
                break;
            default:
                weatherIcon = R.mipmap.ic_launcher;
        }
        return weatherIcon;

    }

}
