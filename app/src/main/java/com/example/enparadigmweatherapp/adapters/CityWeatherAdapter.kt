package com.example.enparadigmweatherapp.adapters

import android.app.Activity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.example.enparadigmweatherapp.R
import com.example.enparadigmweatherapp.adapters.CityWeatherAdapter.MyViewHolder
import com.example.enparadigmweatherapp.models.List1
import com.example.enparadigmweatherapp.utils.ConstantVariables.Companion.convertKelvinToCelcius
import com.example.enparadigmweatherapp.utils.ConstantVariables.Companion.dateConversion
import com.example.enparadigmweatherapp.utils.IconProvider
import com.squareup.picasso.Picasso

/**
 * Created by MartinRuiz on 8/19/2017.
 */
class CityWeatherAdapter(private var list1s: List<List1>, private val layoutReference: Int, private val activity: Activity, private val onItemClickListener: OnItemClickListener) : RecyclerView.Adapter<MyViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view = LayoutInflater.from(activity).inflate(layoutReference, parent, false)
        return MyViewHolder(view)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val list1 = list1s[position]
        holder.tvday.text = dateConversion(list1.dt)
        holder.textViewWeatherDescription.text = list1.weather[0].description
        holder.textViewCurrentTemp.text = convertKelvinToCelcius(list1.main.temp).toString() + "°"
        holder.textViewMaxTemp.text = convertKelvinToCelcius(list1.main.gettemp_max()).toString() + "°"
        holder.textViewMinTemp.text = convertKelvinToCelcius(list1.main.gettemp_min()).toString() + "°"
        val weatherDescription = list1.weather[0].main
        Picasso.with(activity).load(IconProvider.getImageIcon(weatherDescription))
            .into(holder.imageViewWeatherIcon)
        holder.cardViewWeather.setOnClickListener {
            onItemClickListener.onItemClick(
                list1,
                position,
                holder.cardViewWeather
            )
        }
    }

    override fun getItemCount(): Int {
        return list1s.size
    }

    fun notifyUpdate(list1st: List<List1>) {
        list1s = list1st
        notifyDataSetChanged()
    }

    interface OnItemClickListener {
        fun onItemClick(cityWeather: List1?, position: Int, view: View?)
    }

    class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView.rootView) {
        var tvday: TextView = itemView.findViewById(R.id.tv_day)
        var textViewWeatherDescription: TextView = itemView.findViewById(R.id.textViewCardWeatherDescription)
        var textViewCurrentTemp: TextView = itemView.findViewById(R.id.textViewCardCurrentTemp)
        var textViewMaxTemp: TextView = itemView.findViewById(R.id.textViewCardMaxTemp)
        var textViewMinTemp: TextView = itemView.findViewById(R.id.textViewCardMinTemp)
        var imageViewWeatherIcon: ImageView = itemView.findViewById(R.id.imageViewCardWeatherIcon)
        var cardViewWeather: CardView = itemView.findViewById(R.id.cardViewWeatherCard)

    }
}